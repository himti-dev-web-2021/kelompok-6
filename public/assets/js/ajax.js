$(function() {

    function csrfToken() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
    $(document).ready(function() {
        $('#submit').on('click', function(e) {
            e.preventDefault() // Mencegah formnya ke refresh
            const email = $('#email').val() // Mengambil value dari field role name
            console.log(email)
            csrfToken() // Memanggil token Laravel
            try {
                $.ajax({
                    url: 'signup',
                    type: 'POST',
                    dataType: 'json',
                    async: true,
                    data: {
                        email
                    },
                    error: function(error) {
                        console.error(error)
                        alert(error.message)
                        return
                    },
                    success: function(response) {
                        console.log(response)
                        alert("Data Anda" + " " + response.message + " " + "masuk ke database kami")
                        return
                    }
                })
            } catch (error) {
                console.error(error)
                alert(error.message)
                return
            }

        })
    })
})