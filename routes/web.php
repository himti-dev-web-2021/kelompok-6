<?php

use App\Http\Controllers\LoginSignupController;
use App\Http\Controllers\LoginSignupadminController;
use App\Http\Controllers\LoginSignupmitraController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BahanPokokController;
use App\Http\Controllers\DetailController;
use App\Http\Controllers\DetailPemesananController;
use App\Http\Controllers\DetilController;
use App\Http\Controllers\KeranjangController;
use App\Http\Controllers\ListmitraController;
use App\Http\Controllers\PokokController;
use App\Http\Controllers\MitraController;
use App\Http\Controllers\Mitra2Controller;
use App\Models\bahan_pokok;
use Database\Seeders\BahanPokokSeeder;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//user

Route::group(['middleware' => 'AuthCheck'], function () {
    Route::get('/Login', [LoginSignupController::class, 'index']);
    Route::get('/', [LoginSignupController::class, 'indexnoID']);
    Route::get('/dashboarduser', [LoginSignupController::class, 'dashboard']);
    Route::get('/dashboard', [LoginSignupController::class, 'dashboardadmin']);
});
Route::get('/Signup', [LoginSignupController::class, 'signupUI']);
Route::get('auth/google', [LoginSignupController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [LoginSignupController::class, 'handleGoogleCallback']);
Route::get('/authlogin', [LoginSignupController::class, 'login']);
Route::get('/authsignup', [LoginSignupController::class, 'signup']);
Route::get('/keluar', [LoginSignupController::class, 'signout']);



Route::group(['middleware' => 'AuthCheck'], function () {
    Route::get('/Login', [LoginSignupController::class, 'index']);
    Route::get('/', [LoginSignupController::class, 'indexnoID']);
    Route::get('/dashboarduser', [LoginSignupController::class, 'dashboard']);
    Route::get('/dashboard', [LoginSignupController::class, 'dashboardadmin']);
});
Route::get('auth/google', [LoginSignupController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [LoginSignupController::class, 'handleGoogleCallback']);
Route::get('/authlogin', [LoginSignupController::class, 'login']);
Route::get('/authsignup', [LoginSignupController::class, 'signup']);
Route::get('/keluar', [LoginSignupController::class, 'signout']);
Route::get('/Signup', [LoginSignupController::class, 'signupUI']);

//raffi
Route::get('/produk', function () {
    return view('layout/produk');
});

Route::get('/pembayaran', function () {
    return view('pembayaran');
});


Route::get('layout/tambah1', function () {
    return view('layout/tambah1');
});
Route::get('mitra', [MitraController::class, 'index']);
Route::get('/tambahMitra', [MitraController::class, 'create']);
Route::post('mitra', [MitraController::class, 'store1']);
Route::delete('delete1/{no}', [MitraController::class, 'delete1']);
Route::get('edit1/{no}', [MitraController::class, 'edit1']);
Route::patch('update1/{no}', [MitraController::class, 'update1']);
// route::get('dashboardMitra/', [Mitra2Controller::class, 'checkResi']);

Route::get('/tambah1', [BahanPokokController::class, 'create']);
Route::get('layout/produk', [BahanPokokController::class, 'index']);
Route::post('layout/produk', [BahanPokokController::class, 'store']);
Route::delete('/delete/{kode_produk}', [BahanPokokController::class, 'delete']);
Route::get('edit/{kode_produk}', [BahanPokokController::class, 'edit']);
Route::patch('update/{kode_produk}', [BahanPokokController::class, 'update']);


Route::get('/signout', [LoginSignupadminController::class, 'signout']);
Route::get('/signupadmin', [LoginSignupadminController::class, 'signup']);
Route::get('/authadmin', [LoginSignupadminController::class, 'login']);

Route::get('/BahanPokokID', [PokokController::class, 'show']);
Route::get('/BahanPokok', [PokokController::class, 'shownoID']);
Route::get('/detailorder', [KeranjangController::class, 'detailProduk']);
//endRaffi

Route::get('/tambahData', function () {
    return view('layout/tambahData');
});
Route::get('Detail_ID/{kode_produk}', [DetailController::class, 'detail']);
Route::post('Detail_ID/{kode_produk}', [DetailController::class, 'post']);
Route::get('Detail/{kode_produk}', [DetailController::class, 'detailnoID']);
Route::post('Detail/{kode_produk}', [DetailController::class, 'postnoID']);
Route::get('/listmitra', [ListmitraController::class, 'index']);
Route::get('/listmitraID', [ListmitraController::class, 'indexID']);

//Mitra
Route::group(['middleware' => 'AuthMitra'], function () {
    route::get('dashboardMitra/', [Mitra2Controller::class, 'checkResi']);
    Route::get('/LoginMitra', [LoginSignupmitraController::class, 'loginmitra']);
    Route::get('/SignupMitra', [LoginSignupmitraController::class, 'signupmitra']);
});

//mitra
Route::get('/signupmitra', [LoginSignupmitraController::class, 'signup']);
Route::get('/authmitra', [LoginSignupmitraController::class, 'login']);
Route::get('/signoutmitra', [LoginSignupmitraController::class, 'signout']);

//admin
Route::get('/SignupAdmin', [LoginSignupadminController::class, 'signup']);
Route::group(['middleware' => 'AuthAdmin',], function () {
    Route::get('/LoginAdmin', [LoginSignupadminController::class, 'login']);
    Route::get('/dashboard', [LoginSignupadminController::class, 'dashboard']);
});

// admin
Route::get('/SignupAdmin1', [LoginSignupadminController::class, 'signupUI']);
Route::group(['middleware' => 'AuthAdmin',], function () {
    Route::get('/LoginAdmin1', [LoginSignupadminController::class, 'loginUI']);
    Route::get('/dashboard', [LoginSignupadminController::class, 'dashboard']);
});

Route::get('/auth', [LoginSignupadminController::class, 'login']);
Route::get('signup', [LoginSignupadminController::class, 'signup']);
Route::get('/signout', [LoginSignupadminController::class, 'signout']);

//Keranjang
Route::group(['middleware' => 'AuthKeranjang'], function () {
    Route::get('Keranjang', [KeranjangController::class, 'show']);
    Route::get('detailorder', [DetailPemesananController::class, 'tampilProduk']);
});

Route::post('auth/detailorder', [DetailPemesananController::class, 'datauser']);
Route::get('detailorder', [DetailPemesananController::class, 'tampilProduk']);
Route::delete('delete3/{kode_produk}', [KeranjangController::class, 'delete3']);
