<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BahanPokokSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bahan_pokok')->insert([
            [
                'nama_produk' => 'Beras Topi Koki',
                'harga_awal' => '10000',
                'harga_diskon' => '9500',
                'gambar' => 'beras.png',
                'keterangan' => 'per KG',
            ],
            [

                'nama_produk' => 'Telur',
                'harga_awal' => '12000',
                'harga_diskon' => '11500',
                'gambar' => 'telur.png',
                'keterangan' => 'per Butir',
            ],
            [

                'nama_produk' => 'susu',
                'harga_awal' => '15000',
                'harga_diskon' => '14000',
                'gambar' => 'susu.png',
                'keterangan' => 'per liter',
            ],
            [

                'nama_produk' => 'Minyak Goreng',
                'harga_awal' => '13000',
                'harga_diskon' => '12000',
                'gambar' => 'minyak.png',
                'keterangan' => 'per liter',
            ],
            [

                'nama_produk' => 'Gula Pasir',
                'harga_awal' => '12000',
                'harga_diskon' => '11500',
                'gambar' => 'bahan_pokok.png',
                'keterangan' => 'per KG',
            ]
        ]);
    }
}
