<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MitraBekasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mitra_bekasi')->insert([
            [
                'nama_toko' => 'Warung Cak Imin',
                'alamat' => 'jl. Kelapa, Harapan Jaya, Bekasi Utara, Kota Bekasi',
                'no_telpon' => '+6289973629917',
            ],
            [
                'nama_toko' => 'Warung H Udin',
                'alamat' => 'jl. Anggrek, Harapan Jaya, Bekasi Utara, Kota Bekasi',
                'no_telpon' => '+6285378652290',
            ],
            [
                'nama_toko' => 'Warung H Naim',
                'alamat' => 'jl. Kaktus, Harapan Jaya, Bekasi Utara, Kota Bekasi',
                'no_telpon' => '+6288962881663',
            ],
            [
                'nama_toko' => 'Warung Mat Solar',
                'alamat' => 'jl. Pesanggrahan, Petukangan, Jakarta Selatan',
                'no_telpon' => '+6287772653722',
            ],
            [
                'nama_toko' => 'Warung Hj. Yani',
                'alamat' => 'jl. Ciledug Raya, Petukangan Selatan, Jakarta Selatan',
                'no_telpon' => '+6287599382001',
            ],
            [
                'nama_toko' => 'Warung Mewah',
                'alamat' => 'jl. Terogong Raya, Cilandak, Jakarta Selatan',
                'no_telpon' => '+6287728938802',
            ],
            [
                'nama_toko' => 'Warung Musangking',
                'alamat' => 'jl. Kencana Indah, Kebayoran Lama, Jakarta Selatan',
                'no_telpon' => '+6289987654403',
            ]
        ]);
    }
}
