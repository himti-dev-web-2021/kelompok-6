<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBahanPokokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahan_pokok', function (Blueprint $table) {
            $table->id('kode_produk');
            $table->string('nama_produk', 50);
            $table->string('harga_awal', 10);
            $table->string('harga_diskon', 10);
            $table->text('gambar')->nullable;
            $table->text('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahan_pokok');
    }
}
