<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <script src="https://kit.fontawesome.com/3fc8c5566e.js" crossorigin="anonymous"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

  <title>KELONTOENG</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-success fixed-top">
    <div class="container">
      <a class="navbar-brand  fw-bold text-white" href="{{ url('/') }}">KELONTOENG</a>
      <div style="color: white;">
        @php
        if(Session::has('id')){
        echo Session::get('name');
        }else{
        echo "unknown";
        }
        @endphp
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
        <ul class="navbar-nav ml-auto ">
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light" type="submit"><i class="fas fa-search"></i></button>
          </form>
          <li class="nav-item">
            <a class="nav-link text-white js-scroll-trigger" href="Keranjang"><i class="fas fa-shopping-cart"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white js-scroll-trigger" href="/keluar"><span class="badge bg-danger text-white">keluar</span></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container p-5">
    <div class="row">
      @foreach ($bahan_pokok as $item)
      <div class="col">
        <div class="card mt-5" style="width: 15rem;">
          <img src="assets/img/{{ $item->gambar}}" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">{{ $item->nama_produk}}</h5>
            <p class="card-text">
            <h6 class="text-decoration-line-through fw-light"><span class="badge bg-success text-white">20%</span>Rp. {{ $item->harga_awal}}</h6>
            </p>
            <h5>Rp. {{ $item->harga_diskon}}</h5>
            {{ $item->keterangan}}</p>
            <a href="{{url('Detail_ID')}}/{{$item -> kode_produk}}" class="btn btn-primary"><i class="fas fa-cart-plus"></i> Beli</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>

</html>