<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/3fc8c5566e.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="assets/style.css">

    <title>KELONTOENG</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-success fixed-top">
        <div class="container">
            <a class="navbar-brand  fw-bold text-white" href="#">KELONTOENG</a>
            <div style="color: white;">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
                <ul class="navbar-nav ml-auto ">
                    <li class="nav-item active">
                        <a class="nav-link text-white js-scroll-trigger" href="/">Home
                            <i class="fas fa-home"></i>
                            <span class="sr-only">(current)</span></a>
                    <li class="nav-item">
                        <a class="nav-link text-white js-scroll-trigger" href="/keluar"><span class="badge bg-danger text-white">keluar</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <br>

    <div class="container">
        <form action="{{ url('auth/detailorder') }}" method="POST">
            {{csrf_field()}}
            <div class="col md-12">
                <div class="card">
                    <div class="card-body">
                        <h2><i class="fas fa-shopping-cart"></i> Detail Pemesanan</h2>
                        <div class="position-relative">
                            <div class="position-absolute top-0 end-0">Tanggal Pemesanan :
                                <?php
                                date_default_timezone_set('Asia/Jakarta');
                                echo date("d/m/Y");
                                ?>
                            </div>
                        </div>
                        <table class="table table-stripped">
                            <?php
                            ?>
                            </tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td> No ID Pemesanan</td>
                                <td></td>
                                <td>:</td>
                                <td><input type="hidden" name="user_id" value="{{ $user_id }}">
                                    {{ $user_id }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Nama Lengkap</td>
                                <td></td>
                                <td>:</td>
                                <td>
                                    @php
                                    if(Session::has('id')){
                                    echo Session::get('name');
                                    }else{
                                    echo "unknown";
                                    }
                                    @endphp
                                    <input type="hidden" name="name" value="{{ session::get('name')}}">
                                    <input type="hidden" name="id_user" value="{{ session::get('id') }}">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Email</td>
                                <td></td>
                                <td>:</td>
                                <td>
                                    @php
                                    if(Session::has('id')){
                                    echo Session::get('email');
                                    }else{
                                    echo "unknown";
                                    }
                                    @endphp
                                </td>
                            </tr>
                            <button type="button" class="btn btn-success center-block" data-bs-toggle="modal" data-bs-target="#exampleModal">Bayar Sekarang</button>
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Informasi Pembayaran</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="accordion" id="accordionExample">
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingfirst">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefirst" aria-expanded="false" aria-controls="collapsefirst">
                                                            Bank BRI
                                                        </button>
                                                    </h2>
                                                    <div id="collapsefirst" class="accordion-collapse collapse" aria-labelledby="headingfirst" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Ikuti Langkah Berikut.</strong> <br>
                                                            1. Masukkan PIN <br>
                                                            2. Pilih Transaksi Lainnya > Transfer > Ke Rek Bank Lain <br>
                                                            3. Masukkan kode bank BRI 016 <br>
                                                            4. Masukkan No. Rekening <strong>2343-567-45556</strong> diikuti dengan ID Pemesanan <input type="hidden" name="user_id" value="{{ $user_id }}"><strong> {{ $user_id }} </strong> <br>
                                                            5. Masukkan jumlah pembayaran <br>
                                                            6. Pastikan Anda telah memasukkan No Rekening diikuti dengan Kode Ref. Pembayaran dan Nominal Pembayaran dengan Benar <br>
                                                            7. Jika sudah benar, tekan "Benar"
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingTwo">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                            Bank Mandiri
                                                        </button>
                                                    </h2>
                                                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Ikuti Langkah Berikut.</strong> <br>
                                                            1. Masukkan PIN <br>
                                                            2. Pilih Transaksi Lainnya > Transfer > Ke Rek Bank Lain <br>
                                                            3. Masukkan kode bank Mandiri 008 <br>
                                                            4. Masukkan No. Rekening <strong>14-5444-89-0</strong> diikuti dengan ID Pemesanan <input type="hidden" name="user_id" value="{{ $user_id }}"><strong> {{ $user_id }} </strong> <br>
                                                            5. Masukkan jumlah pembayaran <br>
                                                            6. Pastikan Anda telah memasukkan No Rekening diikuti dengan Kode Ref. Pembayaran dan Nominal Pembayaran dengan Benar <br>
                                                            7. Jika sudah benar, tekan "Benar"
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingThree">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                            Bank BCA
                                                        </button>
                                                    </h2>
                                                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Ikuti Langkah Berikut.</strong> <br>
                                                            1. Masukkan PIN <br>
                                                            2. Pilih Transaksi Lainnya > Transfer > Ke Rek Bank Lain <br>
                                                            3. Masukkan kode bank BCA 001 <br>
                                                            4. Masukkan No. Rekening <strong>112-334-333</strong> diikuti dengan ID Pemesanan <input type="hidden" name="user_id" value="{{ $user_id }}"><strong> {{ $user_id }} </strong> <br>
                                                            5. Masukkan jumlah pembayaran <br>
                                                            6. Pastikan Anda telah memasukkan No Rekening diikuti dengan Kode Ref. Pembayaran dan Nominal Pembayaran dengan Benar <br>
                                                            7. Jika sudah benar, tekan "Benar"
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingThree">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                            Bank BTN
                                                        </button>
                                                    </h2>
                                                    <div id="collapsefour" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Ikuti Langkah Berikut.</strong> <br>
                                                            1. Masukkan PIN <br>
                                                            2. Pilih Transaksi Lainnya > Transfer > Ke Rek Bank Lain <br>
                                                            3. Masukkan kode bank BTN 004 <br>
                                                            4. Masukkan No. Rekening <strong>34-4444-3-21</strong> diikuti dengan ID Pemesanan <input type="hidden" name="user_id" value="{{ $user_id }}"><strong> {{ $user_id }} </strong> <br>
                                                            5. Masukkan jumlah pembayaran <br>
                                                            6. Pastikan Anda telah memasukkan No Rekening diikuti dengan Kode Ref. Pembayaran dan Nominal Pembayaran dengan Benar <br>
                                                            7. Jika sudah benar, tekan "Benar"
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingThree">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                                            Bank BTPN
                                                        </button>
                                                    </h2>
                                                    <div id="collapsefive" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Ikuti Langkah Berikut.</strong> <br>
                                                            1. Masukkan PIN <br>
                                                            2. Pilih Transaksi Lainnya > Transfer > Ke Rek Bank Lain <br>
                                                            3. Masukkan kode bank BTPN 020 <br>
                                                            4. Masukkan No. Rekening <strong>12-33333-232-11</strong> diikuti dengan ID Pemesanan <input type="hidden" name="user_id" value="{{ $user_id }}"><strong> {{ $user_id }} </strong> <br>
                                                            5. Masukkan jumlah pembayaran <br>
                                                            6. Pastikan Anda telah memasukkan No Rekening diikuti dengan Kode Ref. Pembayaran dan Nominal Pembayaran dengan Benar <br>
                                                            7. Jika sudah benar, tekan "Benar"
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingThree">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                                            Gopay
                                                        </button>
                                                    </h2>
                                                    <div id="collapsesix" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Under Development</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingThree">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                                                            OVO
                                                        </button>
                                                    </h2>
                                                    <div id="collapseseven" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Under Development</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingThree">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                                                            ShoopePay
                                                        </button>
                                                    </h2>
                                                    <div id="collapseeight" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <strong>Under Development</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Ok</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                </tr>
                                <br>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h2><i class="fas fa-shopping-cart"></i> Detail Produk</h2>
                        <table class="table table-stripped">
                            <thead>
                                <th>No</th>
                                <th>Gambar</th>
                                <th>Nama Barang</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                $harga_total = 0; ?>
                                @foreach($pesanans as $pesanans)
                                <?php $subtotal = $pesanans["total_harga"] ?>
                                <tr>
                                    <td>
                                        {{$no++}}
                                    </td>

                                    <td scope="col">
                                        <img src="{{asset('assets/img/'.$pesanans->gambar)}}" class="" width="100px" alt="">
                                    </td>
                                    <td scope="col">

                                        <strong>{{$pesanans -> nama_produk}}</strong>
                                        <input type="hidden" name="nama_produk" value="{{ $pesanans -> nama_produk }}">
                                        <input type="hidden" name="id_produk" value="{{ $pesanans -> id_produk}}">
                                    </td>
                                    <td scope="col">{{$pesanans -> jumlah}}</td>
                                    <input type="hidden" name="jumlah" value="{{ $pesanans -> jumlah }}">
                                    <td scope="col">Rp {{$pesanans -> total_harga}}</td>
                                    <input type="hidden" name="total_harga" value="{{ $pesanans -> total_harga }}">
                                </tr>
                                <?php $harga_total += $subtotal ?>
                                @endforeach
                                <tr>
                                    <td><strong>Total Harga : </strong></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Rp. {{$harga_total}}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </form>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>

</html>