<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <script src="https://kit.fontawesome.com/3fc8c5566e.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="assets/style.css">

  <title>KELONTOENG</title>
</head>

<body>
  <br>
  <nav class="navbar navbar-expand-lg navbar-light bg-success fixed-top">
    <div class="container">
      <a class="navbar-brand  fw-bold text-white" href="{{ url('/') }}">KELONTOENG</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
        <ul class="navbar-nav ml-auto ">
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light" type="submit"><i class="fas fa-search"></i></button>
          </form>
          <li class="nav-item">
            <a class="nav-link text-white js-scroll-trigger" href="Keranjang"><i class="fas fa-shopping-cart"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white js-scroll-trigger" href="/Login"><span class="badge bg-danger text-white">Masuk</span></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>


  <div class="container">
    <div id="carouselExampleIndicators" class="carousel slide p-5" data-bs-ride="carousel">
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="assets/img/poster1.png" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="assets/img/poster3.png" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="assets/img/poster4.png" class="d-block w-100" alt="...">
        </div>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
  </div>

  <div class="container-md-fluid" style="align-content: center;">
    <div class="row bg-white">
      <div class="col-md-3">
        <div class="card" style="width: 18rem; margin-left: 100px">
          <img src="assets/img/Bumbu_dapur.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Bahan Pokok</h5>
            <p class="card-text" style="text-align: justify;">Temukan Berbagai Bahan Pokok untuk kebutuhan anda</p>
            <a href="#" class="btn btn-success">Cek Disini</a>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card" style="width: 18rem; margin-left: 180px">
          <img src="assets/img/location.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Mau Pesan Dimana?</h5>
            <p class="card-text">Cek ketersediaan mitra kami di daerah kalian disini </p>
            <a href="/listmitra" class="btn btn-success">Cek Disini</a>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card" style="width: 19rem; margin-left: 280px">
          <img src="assets/img/logo_promosii.png" class="card-img-top" alt="responsive image">
          <div class="card-body">
            <h5 class="card-title">KATALOG PROMOSI</h5>
            <p class="card-text">Lihat promosi - promosi dari tempat makan favorit kamu disini!</p>
            <p> </p>
            <a href="Katalog" class="btn btn-success">Katalog Promosi</a>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col">
          <h3>Kategori <a href=""><small class="text-success">Lihat Semua</small></a>
          </h3>
        </div>
      </div>
    </div>

    <div class="container-xl me-auto">
      <div class="col-md-12">
        <div class="row">


          <div class="col-md-3">
            <a href="#">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/bahan_kue.jpg" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title" style="color:black">Bahan Kue</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>


          <div class="col-md-3">
            <a href="{{url('/BahanPokok')}}">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/bahan_pokok.png" style="width: 100px; height: 100px;" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title" style="color:black">Bahan
                        Pokok</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-3">
            <a href="#">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/bumbu_masakan.jpg" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title" style="color:black">Bumbu Masakan</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-3">
            <a href="#">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/cemilan.png" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title" style="color:black">Cemilan</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-3">
            <a href="#">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/kopi-susu.png" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title" style="color:black">Kopi dan Susu</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-3">
            <a href="#">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/bahan_kue.png" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title" style="color:black">Sembako</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-3">
            <a href="#">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/Frozen_food.png" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title" style="color:black">Makanan Beku</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-3">
            <a href="#">
              <div class="card mb-3">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="assets/img/Makanan_kaleng.png" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="text-center" style="color:black">Makanan Kaleng</h5>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

        </div>
      </div>
    </div>



    <div class="container">
      <div class="row">
        <div class="col">
          <h3>Rekomendasi Untuk Anda <a href="user/BahanPokok"><small class="text-success">Lihat Semua</small></a> </h3>
        </div>
      </div>

      <div class="row">
        <div class="col">
          <div class="card">
            <img src="assets/img/snack.png" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Snack</h5>
              <p class="card-text">
              <h6 class="text-decoration-line-through fw-light"><span class="badge bg-success text-white">20%</span><small>Rp. 5000</small></h6>
              </p>
              <h5>Rp. 4000</h5>
              </p>
              <a href="#" class="btn btn-primary">Tambah Ke Keranjang</a>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card">
            <img src="assets/img/bumbu_masakan.png" class="card-img-top align-center" alt="...">
            <div class="card-body">
              <h5 class="card-title">Bumbu Masak</h5>
              <p class="card-text">
              <h6 class="text-decoration-line-through fw-light"><span class="badge bg-success text-white">20%</span><small>Rp. 13.000</small></h6>
              </p>
              <h5>Rp. 10.000</h5>
              </p>
              <a href="#" class="btn btn-primary">Tambah Ke Keranjang</a>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card">
            <img src="assets/img/minyak.png" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Minyak</h5>
              <p class="card-text">
              <h6 class="text-decoration-line-through fw-light"><span class="badge bg-success text-white">20%</span><small>Rp. 16.000</small></h6>
              </p>
              <h5>Rp. 12.800</h5>
              </p>
              </p>
              <a href="#" class="btn btn-primary">Tambah Ke Keranjang</a>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card">
            <img src="assets/img/susu.png" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Susu Segar</h5>
              <p class="card-text">
              <h6 class="text-decoration-line-through fw-light"><span class="badge bg-success text-white">20%</span><small> Rp. 15.000</small></h6>
              </p>
              <h5>Rp. 12.000</h5>
              </p>
              </p>
              <a href="#" class="btn btn-primary">Tambah Ke Keranjang</a>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card">
            <img src="assets/img/telur.png" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Telur per Kg</h5>
              <p class="card-text">
              <h6 class="text-decoration-line-through fw-light"><span class="badge bg-success text-white">20%</span><small>Rp. 25.000</small></h6>
              </p>
              <h5>Rp. 20.000</h5>
              </p>
              </p>
              <a href="#" class="btn btn-primary">Tambah Ke Keranjang</a>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card">
            <img src="assets/img/beras.png" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Beras per Kg</h5>
              <p class="card-text">
              <h6 class="text-decoration-line-through fw-light"><span class="badge bg-success text-white">20%</span><small>Rp. 10.000</small></h6>
              </p>
              <h5>Rp. 9500</h5>
              </p>
              </p>
              <a href="#" class="btn btn-primary">Tambah Ke Keranjang</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="footer-text-center bg-success">
      <h1 class="page-section-heading text-center mb-10 text-white">ABOUT <i class="fas fa-info-circle"></i></h1>
      <div class="container">
        <!-- Contact Section Heading-->
        <div class="row">
          <div class="col-md-6 align-self-center">
            <div class="text-center">
              <img src="{{ asset('assets/img/KELONTOENG (1).png') }}" style="width: 250px" height="250px">
            </div>
          </div>
          <div class="col align-self-center">
            <div class="row justify-content-evenly text-white">
              <div class="col-md-4">
                <h5>Admin</h5>
                <p>Atur website bersama kami
                  segera bergabung
                </p>
                <a href="/LoginAdmin1">
                  <p class="text-white">Masuk Admin</P>
                </a>
              </div>
              <div class="col-md-4">
                <h5>Mitra Aplikasi</h5>
                <p>Cara Bergabung
                  Langkah Usaha</p>
                <a href="/LoginMitra">
                  <p class="text-white">Masuk Mitra</P>
                </a>
              </div>
              <div class="col-md-4">
                <h5>Beli</h5>
                <p>Bahan
                  Pokok
                  Snack
                  Makanan Segar
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <h3 class="page-section-heading text-center mb-10 text-white">Contact Us<p>
          <i class="fab fa-instagram  rounded-lg"></i> <i class="fab fa-whatsapp"></i> <i class="fab fa-twitter"></i>
        </p>
      </h3>
      <div class="container">
        <div class="row">
          <div class="col-6 col-md-4">
            <p><small>
                <h5 class="text-center text-white">Raffyanda Riskaputra</h5>
            </p>
            <p class="text-center"><i class="fab fa-instagram text-white"> @raffyanda_r </i> <i class="fab fa-whatsapp text-white"> +6282121870867</i></small></p>
          </div>
          <div class="col-6 col-md-4">
            <p><small>
                <h5 class="text-center text-white">Muhammad Arya Dhika</h5>
            </p>
            <p class="text-center"><i class="fab fa-instagram text-white"> @aryadhika </i> <i class="fab fa-whatsapp text-white"> +6285920602316</i></small></p>
          </div>
          <div class="col-6 col-md-4">
            <p><small>
                <h5 class="text-center text-white">Anugrah Pramesta</h5>
            </p>
            <p class="text-center"><i class="fab fa-instagram text-white"> @anugrahunay </i> <i class="fab fa-whatsapp text-white"> +6281269770082</i></small></p>
          </div>
        </div>
      </div>
  </div>
  </div>


  </footer>



  <!-- !-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
-->
</body>

</html>