@extends('layout/main')

@section('content-wrapper')
<div class="row">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Data Produk</h1>
		</div>
	</div>
</div>
@endsection

@section('content')
<section class="content">
	<h5><i class="fas fa-edit"></i>PRODUK</h5>

	<table class="table table-dark text-center">
		<thead>
			<tr>
				<th scope="col">Kode Produk</th>
				<th scope="col">Nama Produk</th>
				<th scope="col">Harga Awal</th>
				<th scope="col">Harga Diskon</th>
				<th scope="col">Keterangan</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($bahan_pokok as $item)
			<tr>
				<td>{{ $item->kode_produk}}</td>
				<td>{{ $item->nama_produk}}</td>
				<td>{{ $item->harga_awal}}</td>
				<td>{{ $item->harga_diskon}}</td>
				<td>{{ $item->keterangan}}</td>
				<td><a href="{{ url('edit/'.$item->kode_produk) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a></td>
				<td>
					<form action="{{ url('delete/'.$item->kode_produk) }}" method="post" onsubmit="return confirm('Apakah Anda Yakin ingin Menghapus {{ $item->nama_produk}} ?')">
						@method('delete')
						@csrf
						<button class="btn btn-danger">
							<i class="fas fa-trash-alt"></i>
						</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<a href="/tambah1" class="btn btn-primary mt-3">Tambah Data</a>



	@if (session('status'))
	<div class="alert alert-success">
		{{ session('status') }}
	</div>
	@endif
</section>

@endsection
@extends('layout/footer')