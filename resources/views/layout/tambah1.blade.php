@extends('layout/main')

@section('content-wrapper')
<div class="row">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Tambah Data Produk</h1>
		</div>
	</div>
</div>
@endsection

@section('content')
<section class="content">

	<form method="post" action="layout/produk">
		@csrf
		<div class="form-group">
			<label>Nama produk</label>
			<input type="text" name="nama_produk" class="form-control">
		</div>
		<div class="form-group">
			<label>Harga awal</label>
			<input type="text" name="harga_awal" class="form-control">
		</div>
		<div class="form-group">
			<label>Harga Diskon</label>
			<input type="text" name="harga_diskon" class="form-control">
		</div>
		<div class="form-group">
			<label>keterangan</label>
			<input type="text" name="keterangan" class="form-control">
		</div>
		<div class="form-group">
			<label>Gambar</label>
			<input type="file" name="gambar" class="form-control">
		</div>
		<button type="submit" class="btn btn-primary">Tambah Data!</button>
	</form>
</section>


@endsection
@extends('layout/footer')