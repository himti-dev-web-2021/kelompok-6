@extends('layout/main')

@section('content-wrapper')
<div class="container-fluid">

<div class="row">
    <div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Data Produk</h1>
        </div>
    </div>
    </div>
    @endsection

@section('content')

<div class="row">
    <form method="post" action="{{ url('update/'.$item->kode_produk) }}">
    @method('patch')
    @csrf
    <div class="form-group">
        <label>Nama produk</label>
        <input type="text" name="nama_produk" value="{{ $item->nama_produk }}" class="form-control">
    </div>
    <div class="form-group">
        <label>Harga awal</label>
        <input type="text" name="harga_awal" value="{{ $item->harga_awal }}" class="form-control">
    </div>
    <div class="form-group">
        <label>Harga Diskon</label>
        <input type="text" name="harga_diskon" value="{{ $item->harga_diskon }}" class="form-control">
    </div>
    <div class="form-group">
        <label>keterangan</label>
        <input type="text" name="keterangan" value="{{ $item->keterangan }}" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary mt-3">Simpan!</button>
    </form>
</div>

@endsection
@extends('layout/footer')