<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/3fc8c5566e.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="assets/style.css">

    <title>Lets Dine In</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-success fixed-top">
        <div class="container">
            <a class="navbar-brand  fw-bold text-white" href="{{ url('/dashboarduser') }}">KELONTOENG</a>
            <div style="color: white;">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
                <ul class="navbar-nav ml-auto ">
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-light" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                    <li class="nav-item">
                        <a class="nav-link text-white js-scroll-trigger" href="Keranjang"><i class="fas fa-shopping-cart"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white js-scroll-trigger" href="/Login"><span class="badge bg-danger text-white">Masuk</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h2><i class="fa fa-th-list" aria-hidden="true"></i> List Mitra</h2>
                <table class="table table-light text-center">
                    <thead>
                        <tr>
                            <th scope="col">No Toko</th>
                            <th scope="col">Nama Toko</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">No Telepon</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        @foreach ($mitra_bekasi as $item)
                        <tr>
                            <td>{{ $no++}}</td>
                            <td>{{ $item->nama_toko}}</td>
                            <td>{{ $item->alamat}}</td>
                            <td>{{ $item->no_telpon}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <footer class="footer-text-center bg-success">
        <h1 class="page-section-heading text-center mb-10 text-white">ABOUT <i class="fas fa-info-circle"></i></h1>
        <div class="container">
            <!-- Contact Section Heading-->
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <div class="text-center">
                        <img src="assets/img/KELONTOENG (1).png" style="width: 300px" height="300px">
                    </div>
                </div>
                <div class="col align-self-center">
                    <div class="row justify-content-evenly text-white">
                        <div class="col-md-4">
                            <h5>Admin</h5>
                            <p>Atur website bersama kami
                                segera bergabung
                            </p>
                            <a class="nav-link text-white js-scroll-trigger" href="/LoginAdmin"><span class="badge bg-danger text-white">Masuk Admin</span></a>
                        </div>
                        <div class="col-md-4">
                            <h5>Mitra Aplikasi</h5>
                            <p>Cara Bergabung
                                Langkah Usaha</p>
                            <a class="nav-link text-white js-scroll-trigger" href="/LoginMitra"><span class="badge bg-danger text-white">Masuk Mitra</span></a>
                        </div>
                        <div class="col-md-4">
                            <h5>Beli</h5>
                            <p>Bahan
                                Pokok
                                Snack
                                Makanan Segar
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h3 class="page-section-heading text-center mb-10 text-white">Contact Us<p>
                <i class="fab fa-instagram  rounded-lg"></i> <i class="fab fa-whatsapp"></i> <i class="fab fa-twitter"></i>
            </p>
        </h3>
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-4">
                    <p><small>
                            <h5 class="text-center text-white">Raffyanda Riskaputra</h5>
                    </p>
                    <p class="text-center"><i class="fab fa-instagram text-white"> @raffyanda_r </i> <i class="fab fa-whatsapp text-white"> +6282121870867</i></small></p>
                </div>
                <div class="col-6 col-md-4">
                    <p><small>
                            <h5 class="text-center text-white">Muhammad Arya Dhika</h5>
                    </p>
                    <p class="text-center"><i class="fab fa-instagram text-white"> @aryadhika </i> <i class="fab fa-whatsapp text-white"> +6285920602316</i></small></p>
                </div>
                <div class="col-6 col-md-4">
                    <p><small>
                            <h5 class="text-center text-white">Anugrah Pramesta</h5>
                    </p>
                    <p class="text-center"><i class="fab fa-instagram text-white"> @anugrahunay </i> <i class="fab fa-whatsapp text-white"> +6281269770082</i></small></p>
                </div>
            </div>

    </footer>

    <!-- !-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
-->
</body>

</html>