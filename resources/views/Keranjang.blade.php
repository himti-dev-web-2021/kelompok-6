<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/3fc8c5566e.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="assets/style.css">

    <title>KELONTOENG</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-success fixed-top">
        <div class="container">
            <a class="navbar-brand  fw-bold text-white" href="#">KELONTOENG</a>
            <div style="color: white;">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
                <ul class="navbar-nav ml-auto ">
                </ul>
            </div>
        </div>
    </nav>
    <br>

    <div class="container">
        <div class="col md-12">
            <div class="card">
                <div class="card-body">
                    <h2><i class="fas fa-shopping-cart"></i> Keranjang</h2>
                    <table class="table table-stripped">
                        <thead>
                            <th>No</th>
                            <th>Gambar</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            $harga_total = 0; ?>

                            @foreach($pesanans as $pesanans)
                            <?php $subtotal = $pesanans["total_harga"] ?>
                            <tr>
                                <td>
                                    {{$no++}}
                                </td>

                                <td scope="col">
                                    <img src="{{asset('assets/img/'.$pesanans->gambar)}}" class="" width="100px" alt="">
                                </td>
                                <td scope="col">
                                    <strong>{{$pesanans -> nama_produk}}</strong>
                                </td>
                                <td scope="col">{{$pesanans -> jumlah}}</td>
                                <td scope="col">Rp {{$pesanans -> total_harga}}</td>
                                <td>
                                    <form action="{{ url('delete3/'.$pesanans->id_produk) }}" method="post" onsubmit="return confirm('Apakah Anda Yakin ingin Menghapus {{ $pesanans->nama_produk}} ?')">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <?php $harga_total += $subtotal ?>
                            @endforeach
                        </tbody>
                        <td>
                            Total Harga :
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong> Rp. {{$harga_total}} </strong></td>
                        <td>
                            <a href="{{ url('/detailorder') }}" type="button" class="btn btn-success center-block">CheckOut</a>
                        </td>
                    </table>
                </div>
            </div>

        </div>

        <!-- Optional JavaScript; choose one of the two! -->

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>

</html>