<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="{{ asset('assets/css/LoginSignup.css') }}" />
  <title>Sign in & Sign up Form</title>
</head>

<body>
  <div class="container">
    <div class="forms-container">
      <div class="signin-signup">
        <form action="{{ url('/authsignup') }}" class="sign-in-form">
          @csrf
          @method('POST')
          <h2 class="title">Sign up</h2>
          <div class="input-field">
            <i class="fas fa-user"></i>
            <input type="text" id="name" name="name" value="{{ old('name') }}" placeholder="Username" />
          </div>
          <center><span class="text-danger">@error('name'){{ $message }}@enderror</span></center>
          <div class="input-field">
            <i class="fas fa-envelope"></i>
            <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Email" />
          </div>
          <center><span class="text-danger">@error('email'){{ $message }}@enderror</span></center>
          <div class="input-field">
            <i class="fas fa-lock"></i>
            <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password" />
          </div>
          <center><span class="text-danger">@error('password'){{ $message }}@enderror</span></center>
          @if (Session::get('failed'))
          <div class="alert alert-danger">
            {{ Session::get('failed') }}
          </div>
          @endif
          <input type="submit" class="btn" id="submit" name="submit" value="Sign up" />
          <p class="social-text">Or Sign up with social platforms</p>
          <div class="social-media">
            <a href="#" class="social-icon">
              <i class="fab fa-facebook-f"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-twitter"></i>
            </a>
            <a href="{{ url('auth/google') }}" class="social-icon">
              <i class="fab fa-google"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-linkedin-in"></i>
            </a>
          </div>
        </form>
      </div>
    </div>

    <div class="panels-container">
      <div class="panel left-panel">
        <div class="content">
        <img src="assets/img/KELONTOENG (1).png" style="width: 250px" height="250px">
          <h3>One of us ?</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum
            laboriosam ad deleniti.
          </p>
          <form action="{{ url('Login') }}">
          <button class="btn transparent" id="sign-in-btn">
            Sign in
          </button>
          </form>
        </div>
        <!-- <img src="{{ asset('assets/img/register.svg') }}" class="image" alt="" /> -->
      </div>
    </div>

  <script src="{{ asset('assets/js/LoginSignup.js') }}"></script>
  <script>
    function csrfToken() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    }
    $(document).ready(function() {
      $('#submit').on('click', function(e) {
        e.preventDefault() // Mencegah formnya ke refresh
      })
    })
  </script>
  <!-- <script src="{{ asset('assets/js/ajax.js') }}"></script>  -->
</body>

</html>