<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="{{ asset('assets/css/LoginSignup.css') }}" />
  <title>Sign in & Sign up Form</title>
</head>

<body>
  <div class="container">
    <div class="forms-container">
      <div class="signin-signup">
        <form action="{{ url('/signupmitra') }}" class="sign-in-form">
          @csrf
          @method('POST')
          @if (Session::get('failed'))
          @endif
          <h2 class="title">Sign up Mitra</h2>
          <div class="input-field">
            <i class="fas fa-user"></i>
            <input type="text" id="name" name="name" value="{{ old('name') }}" placeholder="Username" />
          </div>
          <center><span class="text-danger">@error('name'){{ $message }}@enderror</span></center>
          <div class="input-field">
            <i class="fas fa-envelope"></i>
            <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Email" />
          </div>
          <center><span class="text-danger">@error('email'){{ $message }}@enderror</span></center>
          <div class="input-field">
            <i class="fas fa-lock"></i>
            <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password" />
          </div>
          <center><span class="text-danger">@error('password'){{ $message }}@enderror</span></center>
          {{ Session::get('failed') }}
          <input type="submit" class="btn" id="submit" name="submit" value="Sign up" />
      </div>
      </form>
    </div>
  </div>

  <div class="panels-container">
    <div class="panel left-panel">
      <div class="content">
        <h3>One of us ?</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum
          laboriosam ad deleniti.
        </p>
        <form action="{{ url('LoginMitra') }}">
          <button class="btn transparent" id="sign-in-btn">
            Sign in
          </button>
        </form>
      </div>
      <!-- <img src="{{ asset('assets/img/register.svg') }}" class="image" alt="" /> -->
    </div>
  </div>

  <script src="{{ asset('assets/js/LoginSignup.js') }}"></script>
  <script>
    function csrfToken() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    }
    $(document).ready(function() {
      $('#submit').on('click', function(e) {
        e.preventDefault() // Mencegah formnya ke refresh
      })
    })
  </script>
  <!-- <script src="{{ asset('assets/js/ajax.js') }}"></script>  -->
</body>

</html>