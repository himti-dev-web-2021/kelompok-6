@extends('layout/main')

@section('content-wrapper')
<div class="container-fluid">

    <div class="row">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Edit Data Kemitraan</h1>
            </div>
        </div>
    </div>
    @endsection

    @section('content')

    <div class="row">
        <form method="post" action="{{ url('update1/'.$item->no) }}">
            @method('patch')
            @csrf
            <div class="form-group">
                <label>Nama Toko</label>
                <input type="text" name="nama_toko" value="{{ $item->nama_toko }}" class="form-control">
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" name="alamat" value="{{ $item->alamat }}" class="form-control">
            </div>
            <div class="form-group">
                <label>No Telepon</label>
                <input type="text" name="no_telpon" value="{{ $item->no_telpon }}" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary mt-3">Simpan!</button>
        </form>
    </div>

    @endsection
    @extends('layout/footer')