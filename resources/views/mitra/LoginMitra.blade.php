<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="{{ asset('assets/css/LoginSignup.css') }}" />
  <title>Sign in & Sign up Form</title>
</head>

<body>
  <div class="container">
    <div class="forms-container">
      <div class="signin-signup">
        <form action="{{ url('/authmitra') }}" class="sign-in-form">
          @csrf
          @method('POST')
          @if (Session::get('failed'))
          @endif
          <h2 class="title">Sign in Mitra</h2>
          {{ Session::get('ditolak') }}
          <div class="input-field">
            <i class="fas fa-user"></i>
            <input type="text" id="email" name="email" value="{{ old('email') }}" placeholder="email" />
          </div>
          <center><span class="alert alert-danger">@error('email'){{ $message }}@enderror</span></center>
          <div class="alert alert-danger">
            {{ Session::get('failed') }}
          </div>
          <div class="input-field">
            <i class="fas fa-lock"></i>
            <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password" />
          </div>
          <center><span class="text-danger">@error('password'){{ $message }}@enderror</span></center>
          <div class="alert alert-danger">
            {{ Session::get('gagal') }}
          </div>
          <input type="submit" value="Login" id="submit" name="submit" class="btn solid" />
        </form>
      </div>
    </div>

    <div class="panels-container">
      <div class="panel left-panel">
        <div class="content">
          <img src="assets/img/KELONTOENG (1).png" style="width: 250px" height="250px">
          <h3>New here ?</h3>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Debitis,
            ex ratione. Aliquid!
          </p>
          <form action="{{ url('authmitra') }}">
            <button class="btn transparent" id="sign-in-btn">
              Sign up
            </button>
          </form>
        </div>
        <!-- <img src="{{ asset('assets/img/register.svg') }}" class="image" alt="" /> -->
      </div>
    </div>
  </div>

  <script src="{{ asset('assets/js/LoginSignup.js') }}"></script>
  <script>
    function csrfToken() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    }
    $(document).ready(function() {
      $('#submit').on('click', function(e) {
        e.preventDefault() // Mencegah formnya ke refresh
      })
    })
  </script>
  <!-- <script src="{{ asset('assets/js/ajax.js') }}"></script>  -->
</body>

</html>