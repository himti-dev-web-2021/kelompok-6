@extends('layout/main')

@section('content-wrapper')
<div class="row">

	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-clipboard-list mr-2"></i> Data Kemitraan</h1>
		</div>
	</div>

</div>
@endsection

@section('content')
<section class="content">


	<table class="table table-light text-center">
		<thead>
			<tr>
				<th scope="col">No Toko</th>
				<th scope="col">Nama Toko</th>
				<th scope="col">Alamat</th>
				<th scope="col">No Telepon</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1; ?>
			@foreach ($mitra_bekasi as $item)
			<tr>
				<td>{{ $no++}}</td>
				<td>{{ $item->nama_toko}}</td>
				<td>{{ $item->alamat}}</td>
				<td>{{ $item->no_telpon}}</td>
				<td><a href="{{ url('edit1/'.$item->no) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a></td>
				<td>
					<form action="{{ url('delete1/'.$item->no) }}" method="post" onsubmit="return confirm('Apakah Anda Yakin ingin Menghapus {{ $item->nama_toko}} ?')">
						@method('delete')
						@csrf
						<button class="btn btn-danger">
							<i class="fas fa-trash-alt"></i>
						</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<a href="/tambahMitra" class="btn btn-primary mt-3">Tambah Data</a>


	@if (session('status'))
	<div class="alert alert-success">
		{{ session('status') }}
	</div>
	@endif
</section>

@endsection
@extends('layout/footer')