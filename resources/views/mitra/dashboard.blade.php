<!doctype html>
<html lang="en">

<!-- Bootstrap CSS -->
<link href="{{ asset('assets') }}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/3fc8c5566e.js" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="assets/style.css">


<title>KELONTOENG/Mitra</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-success fixed-top">
    <div class="container">
      <a class="navbar-brand  fw-bold text-white" href="#">KELONTOENG ||
        <small>Hallo

        </small>
      </a>@php
      if(Session::has('id')){
      echo Session::get('name');
      }else{
      echo "unknown";
      }
      @endphp
      !
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link text-white js-scroll-trigger" href="">Home
              <i class="fas fa-home"></i>
              <span class="sr-only">(current)</span></a>
          <li class="nav-item dropdown">
            <a class="nav-link text-white" href="#pesan_disini" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Input Resi
            </a>
          </li>
          </li>
          <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
            <ul class="navbar-nav">
              <li class="nav-item dropdown ">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Profile <i class="fas fa-user-circle"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-success" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><a class="dropdown-item" href="#"> <i class="fas fa-user"></i> @php
                      if(Session::has('id')){
                      echo Session::get('name');
                      }else{
                      echo "unknown";
                      }
                      @endphp
                      !</a></li>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/signoutmitra" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                  </a>
                </ul>
              </li>
            </ul>
          </div>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container mt-5 p-3">
    <div class="card bg-success">
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h3 class="text-white">Masukkan No. Resi</h3>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <form action="{{ url('dashboardMitra/') }}" class="form-inline" method="">
            @csrf
            <div class="input-group mb-3 mr-3 ml-3">
              <input name="user_id" type="text" value="{{ $user_id }}" class="form-control" placeholder="Masukkan Resi" aria-label="Recipient's username" aria-describedby="button-addon2">
              <button class="btn btn-danger" type="submit" id="button-addon2">Check</button>
            </div>
          </form>
        </div>
      </div>
      <?php $no = 1; ?>
      @foreach ($Mitra2 as $item)
      <ol class="list-group ">
        <li class="list-group-item d-flex justify-content-between align-items-start bg-success text-white">
          <div class="ms-2 me-auto">
            <div class="fw-bold">{{ $item->nama_produk }}</div>
            Rp. {{ $item->total_harga }}
          </div>
          <span class="badge bg-primary rounded-pill">x{{ $item->jumlah }}</span>
        </li>
      </ol>
      @endforeach
    </div>


  </div>

  <!-- !-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
-->
</body>

</html>
</div>