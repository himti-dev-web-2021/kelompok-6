@extends('layout/main')

@section('content-wrapper')
<div class="row">
  <div class="container-fluid">
<!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Tambah Data Kemitraan</h1>
    </div>
  </div>
</div>
@endsection

@section('content')
	<section class="content">

    <form method="post" action="/mitra">
    @csrf
	<div class="form-group">
    	<label>Nama Toko</label>
        <input type="text" name="nama_toko" class="form-control">
	</div>
	<div class="form-group">
        <label>Alamat</label>
		<input type="text" name="alamat" class="form-control">
   	</div>
	<div class="form-group">
		<label>No Telepon</label>
        <input type="number" name="no_telpon" class="form-control">
	</div>
    <button type="submit" class="btn btn-primary">Tambah Data!</button>
    </form>
	</section>


    @endsection
    @extends('layout/footer')