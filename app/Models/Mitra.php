<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
    protected $fillable = ['nama_toko','alamat','no_telpon'];
    protected $table = 'mitra_bekasi'; 
    public $timestamps = false;
}
