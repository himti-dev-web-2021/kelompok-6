<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PesananDetail extends Model
{
    protected $table = "pesanan_details",
     $fillable = ['id', 'id_user', 'id_produk', 'id_pemesanan'];
    
    //  public function pengguna () {
    //      return $this->belongsTo(pengguna::class, 'name', 'email');
    //  }
}
