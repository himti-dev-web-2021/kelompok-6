<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mitra2 extends Model
{
    protected $fillable = ['user_id', 'id_produk ', 'nama_produk', 'jumlah', 'total_harga'];
    protected $table = 'pesanans';
    public $timestamps = false;
}
