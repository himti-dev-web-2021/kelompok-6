<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    public function pengguna()
    {
        return $this->belongsTo('App\Model\pengguna', 'user_id', 'id');
    }
    public function pesanan_detail()
    {
        return $this->belongsTo('App\Model\PesananDetail', 'pesanan_id', 'id');
    }
}
