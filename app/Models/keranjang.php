<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class keranjang extends Model
{
    protected $fillable = ['user_id', 'id_produk', 'nama_produk', 'gambar', 'tanggal', 'status', 'jumlah', 'total_harga'];
    protected $table = 'pesanans';
    public $timestamps = false;
}
