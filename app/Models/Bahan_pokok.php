<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bahan_pokok extends Model
{
    protected $fillable = ['nama_produk', 'harga_awal', 'harga_diskon', 'gambar', 'keterangan'];
    protected $table = 'bahan_pokok';
    public $timestamps = false;

    public function deleteData($kode_produk)
    {
        DB::table('bahan_pokok')
            ->where('kode_produk', $kode_produk)
            ->delete();
    }

    public function detailData($kode_produk)
    {
        return DB::table('bahan_pokok')
            ->where('kode_produk', $kode_produk)
            ->first();
    }

    public function editData($kode_produk, $item)
    {
        return DB::table('bahan_pokok')
            ->where('kode_produk', $kode_produk)
            ->update($item);
    }
    public function pesanan_detail()
    {
        return $this->hasMany('App\Model\PesananDetail', 'bahan_id', 'id');
    }
}
