<?php

namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use Illuminate\Http\Request;
use App\Models\admin;
use Illuminate\Support\Facades\Hash;

class LoginSignupadminController extends Controller
{
    public function signupUI()
    {
        return view('SignupAdmin');
    }
    public function loginUI()
    {
        return view('/LoginAdmin');
    }
    public function dashboard()
    {
        return view('layout/dashboard');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|min:8|max:20'
        ], [
            'password.min' => 'Password minimal memiliki 8 karakter',
            'password.max' => 'Passwrod maksimal memiliki 20 karakter',
            'password.required' => 'Password anda harus diisikan',
            'email.required' => 'Email anda harus diisikan',
            'email.email' => 'Alamat email tidak valid, gunakan example@gmail.com',
        ]);
        //modelDb::elequent('key', 'value')
        $userInfo = admin::where('email', $request->email)->first();
        if (!$userInfo || empty($userInfo)) {
            return back()->with('gagal', 'Email anda tidak cocok');
        } else {
            $ambil = $userInfo->password;
            $pass = $request->password;
            if (Hash::check($pass, $ambil)) {
                $request->session()->put('id', $userInfo->id);
                $request->session()->put('name', $userInfo->name);
                $request->session()->put('email', $userInfo->email);
                $request->session()->put('password', $userInfo->password);
                return redirect()->intended('/dashboard');
            } else {
                return back()->with('failed', 'Password anda tidak cocok');
            }
        }
    }
    public function signout()
    {
        if (session()->has('id')) {
            session()->pull('id');
            session()->pull('name');
            session()->pull('email');
            session()->pull('password');
            return redirect()->intended('/');
        }
    }
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:admin',
            'password' => 'required|min:8|max:20',
        ], [
            'name.required' => 'Username anda harus diisikan',
            'name.string' => "Username harus bertipe string/kalimat",
            'email.required' => 'Email anda harus diisikan',
            'email.unique' => 'Email anda sudah digunakan',
            'email.email' => 'Email harus berbentuk example@gmail.com',
            'password.required' => 'Password anda harus diisikan',
            'password.min' => 'Password minimal memiliki 8 karakter',
            'password.max' => 'Passwrod maksimal memiliki 20 karakter',
        ]);
        $ambil = admin::all('password')->first();
        if (!empty($ambil)) {
            $pass = $ambil->password;
            if (Hash::check($request->password, $pass)) {
                return back()->with('failed', 'Password anda telah dipakai');
            } else {
                $input = admin::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password)
                ]);
                $check = admin::where('email', $request->email)->first();
                $request->session()->put('id', $check->id);
                $request->session()->put('name', $check->name);
                $request->session()->put('email', $check->email);
                $request->session()->put('password', $check->password);
                return redirect()->intended('/dashboard');
            }
        } else {
            $input = admin::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
            $check = admin::where('email', $request->email)->first();
            $request->session()->put('id', $check->id);
            $request->session()->put('name', $check->name);
            $request->session()->put('email', $check->email);
            $request->session()->put('password', $check->password);
            return redirect()->intended('/dashboard');
        }
    }
}
