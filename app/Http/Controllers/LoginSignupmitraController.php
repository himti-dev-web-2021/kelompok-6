<?php

namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use Illuminate\Http\Request;
use App\Models\MitraPengguna;
use Illuminate\Support\Facades\Hash;

class LoginSignupmitraController extends Controller
{
    public function dashboardmitra()
    {
        return view('mitra/dashboard');
    }
    public function loginmitra()
    {
        return view('mitra/LoginMitra');
    }
    public function signupmitra()
    {
        return view('mitra.SignupMitra');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|min:8|max:20'
        ], [
            'password.min' => 'Password minimal memiliki 8 karakter',
            'password.max' => 'Passwrod maksimal memiliki 20 karakter',
            'password.required' => 'Password anda harus diisikan',
            'email.required' => 'Email anda harus diisikan',
            'email.email' => 'Alamat email tidak valid, gunakan example@gmail.com',
        ]);
        //modelDb::elequent('key', 'value')
        $userInfo = MitraPengguna::where('email', $request->email)->first();
        if (!$userInfo || empty($userInfo)) {
            return back()->with('gagal', 'Email anda tidak cocok');
        } else {
            $ambil = $userInfo->password;
            $pass = $request->password;
            if (Hash::check($pass, $ambil)) {
                $request->session()->put('id', $userInfo->id);
                $request->session()->put('name', $userInfo->name);
                $request->session()->put('email', $userInfo->email);
                $request->session()->put('password', $userInfo->password);
                return redirect()->intended('/dashboardMitra');
            } else {
                return back()->with('failed', 'Password anda tidak cocok');
            }
        }
    }
    public function signout()
    {
        if (session()->has('id')) {
            session()->pull('id');
            session()->pull('name');
            session()->pull('email');
            session()->pull('password');
            return redirect()->intended('/');
        }
    }
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:mitra',
            'password' => 'required|min:8|max:20',
        ], [
            'name.required' => 'Username anda harus diisikan',
            'name.string' => "Username harus bertipe string/kalimat",
            'email.required' => 'Email anda harus diisikan',
            'email.unique' => 'Email anda sudah digunakan',
            'email.email' => 'Email harus berbentuk example@gmail.com',
            'password.required' => 'Password anda harus diisikan',
            'password.min' => 'Password minimal memiliki 8 karakter',
            'password.max' => 'Passwrod maksimal memiliki 20 karakter',
        ]);

        $ambil = MitraPengguna::all('password')->first();
        if (!empty($ambil)) {
            $pass = $ambil->password;
            if (Hash::check($request->password, $pass)) {
                return back()->with('failed', 'Password anda telah dipakai');
            } else {
                $input = MitraPengguna::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password)
                ]);
                $check = MitraPengguna::where('email', $request->email)->first();
                $request->session()->put('id', $check->id);
                $request->session()->put('name', $check->name);
                $request->session()->put('email', $check->email);
                $request->session()->put('password', $check->password);
                return redirect()->intended('/dashboardMitra');
            }
        } else {
            $input = MitraPengguna::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
            $check = MitraPengguna::where('email', $request->email)->first();
            $request->session()->put('id', $check->id);
            $request->session()->put('name', $check->name);
            $request->session()->put('email', $check->email);
            $request->session()->put('password', $check->password);
            return redirect()->intended('/dashboardmitra');
        }
    }
}
