<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\keranjang;
use Illuminate\Support\Facades\DB;

class KeranjangController extends Controller
{

    public function show()
    {
        $pesanans = \App\Models\keranjang::all();

        return view('keranjang', ['pesanans' => $pesanans]);
    }

    public function delete3($id_produk)
    {
        DB::table('pesanans')->where('id_produk', $id_produk)->delete();
        return redirect('/Keranjang')->with('status', 'Produk Berhasil Dihapus!');
    }
    public function detailProduk()
    {
        return view('DetailOrder');
    }
    public function datauser(Request $request)
    {
        DB::table('pesanan_details')->insert([
            'id_user' => $request->id_user,
            'nama_produk' => $request->nama_produk,
            'jumlah' => $request->jumlah,
            'total_harga' => $request->total_harga,
            'id_produk' => $request->id_produk,
            'id_pemesanan' => $request->id_pemesanan
        ]);
    }
}
