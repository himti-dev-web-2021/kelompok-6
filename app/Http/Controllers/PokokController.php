<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PokokController extends Controller
{
    public function show()
    {
        $bahan_pokok = DB::table('bahan_pokok')->get();
        return view('BahanPokokID', ['bahan_pokok' => $bahan_pokok]);
    }
    public function shownoID()
    {
        $bahan_pokok = DB::table('bahan_pokok')->get();
        return view('user.BahanPokok', ['bahan_pokok' => $bahan_pokok]);
    }
}
