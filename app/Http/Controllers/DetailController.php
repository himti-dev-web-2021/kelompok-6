<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bahan_pokok;
use Carbon\Carbon;
use App\Models\Pesanan;
use App\Models\PesananDetail;
use App\Models\pengguna;
use Illuminate\Support\Facades\Auth;

class DetailController extends Controller
{
    public function detail($kode_produk)
    {
        $bahan_pokok = Bahan_pokok::where('kode_produk', $kode_produk)->first();
        return view('DetailID', compact('bahan_pokok'));
    }

    public function post(Request $request, $kode_produk)
    {
        $bahan_pokok = Bahan_pokok::where('kode_produk', $kode_produk)->first();
        $tanggal = Carbon::now();

        $pesanan = new Pesanan;
        $pesanan->user_id = $request->id_user;
        $pesanan->id_produk = $bahan_pokok->kode_produk;
        $pesanan->nama_produk = $bahan_pokok->nama_produk;
        $pesanan->gambar = $bahan_pokok->gambar;
        $pesanan->tanggal = $tanggal;
        $pesanan->status = 0;
        $pesanan->jumlah = $request->jumlah_pesan;
        $pesanan->total_harga = $bahan_pokok->harga_diskon * $request->jumlah_pesan;
        $pesanan->save();
        return redirect('BahanPokokID');
    }
    public function detailnoID($kode_produk)
    {
        $bahan_pokok = Bahan_pokok::where('kode_produk', $kode_produk)->first();
        return view('Detail', compact('bahan_pokok'));
    }
    public function postnoID(Request $request, $kode_produk)
    {
        $bahan_pokok = Bahan_pokok::where('kode_produk', $kode_produk)->first();
        $tanggal = Carbon::now();

        //simpan ke DB pesanan
        $pesanan = new Pesanan;
        $pesanan->id_produk = $bahan_pokok->kode_produk;
        $pesanan->nama_produk = $bahan_pokok->nama_produk;
        $pesanan->gambar = $bahan_pokok->gambar;
        $pesanan->tanggal = $tanggal;
        $pesanan->status = 0;
        $pesanan->jumlah = $request->jumlah_pesan;
        $pesanan->total_harga = $bahan_pokok->harga_diskon * $request->jumlah_pesan;
        $pesanan->save();
        return redirect('BahanPokok');
    }
}
