<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mitra2;

class Mitra2Controller extends Controller
{

    public function __construct()
    {
        $this->Mitra2 = new Mitra2();
    }

    public function checkResi()
    {
        $Mitra2 = Mitra2::where('user_id', Request()->user_id)->get();
        return view('mitra.dashboard', ['user_id' => Request()->user_id])->with(compact('Mitra2'));
    }
}
