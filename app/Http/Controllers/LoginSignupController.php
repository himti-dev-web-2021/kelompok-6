<?php

namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\pengguna;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

use function GuzzleHttp\Promise\all;

class LoginSignupController extends Controller
{
    public function index()
    {
        return view('Login');
    }
    public function dashboard()
    {
        return view('indexID');
    }
    public function indexnoID()
    {
        return view('index');
    }
    public function dashboardadmin()
    {
        return view('layout/dashboard');
    }
    public function signupUI()
    {
        return view('Signup');
    }
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            $finduser = pengguna::where('google_id', $user->id)->first();
            if ($finduser) {
                Auth::login($finduser);
                session()->put('id', $finduser->id);
                session()->put('name', $finduser->name);
                session()->put('email', $finduser->email);
                session()->put('image', $finduser->image);
                session()->put('password', $finduser->password);
                return redirect()->intended('/dashboarduser');
            } else {
                $newUser = pengguna::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'image' => $user->avatar,
                    'google_id' => $user->id,
                    'password' => Hash::make('123456dummy')
                ]);
                session()->put('id', $newUser->id);
                session()->put('name', $newUser->name);
                session()->put('email', $newUser->email);
                session()->put('image', $newUser->image);
                session()->put('password', $newUser->password);

                Auth::login($newUser);

                return redirect()->intended('/dashboarduser');
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|min:8|max:20'
        ], [
            'password.min' => 'Password minimal memiliki 8 karakter',
            'password.required' => 'Password anda harus diisikan',
            'email.required' => 'Email anda harus diisikan',
            'email.email' => 'Alamat email tidak valid, gunakan example@gmail.com',
        ]);
        //modelDb::elequent('key', 'value')
        $userInfo = pengguna::where('email', $request->email)->first();
        if (!$userInfo || empty($userInfo)) {
            return back()->with('failed', 'Email anda tidak cocok');
        } else {
            $request->session()->put('id', $userInfo->id);
            $request->session()->put('name', $userInfo->name);
            $request->session()->put('email', $userInfo->email);
            $request->session()->put('image', $userInfo->image);
            $request->session()->put('password', $userInfo->password);
            return redirect()->intended('/dashboarduser');
        }
    }
    public function signout()
    {
        if (session()->has('id')) {
            session()->pull('id');
            session()->pull('name');
            session()->pull('email');
            session()->pull('image');
            session()->pull('password');
            return redirect()->intended('/');
        }
    }
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:pengguna',
            'password' => 'required|min:8|max:20'
        ], [
            'name.required' => 'Username anda harus diisikan',
            'email.required' => 'Email anda harus diisikan',
            'email.unique' => 'Email anda sudah digunakan',
            'password.required' => 'Password anda harus diisikan',
            'password.min' => 'Password minimal memiliki 8 karakter',
        ]);
        $input = pengguna::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $request->session()->put('id', $input->id);
        $request->session()->put('name', $input->name);
        $request->session()->put('email', $input->email);
        $request->session()->put('password', $input->password);
        return redirect()->intended('/dashboarduser');
    }
}
