<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MitraController extends Controller
{
    public function __construct()
    {
        $this->Mitra = new Mitra();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $mitra_bekasi = \App\Models\Mitra::all();

        return view('mitra.mitra', ['mitra_bekasi' => $mitra_bekasi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mitra.tambahMitra');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $requestx  
     * @return \Illuminate\Http\Response
     */
    public function store1(Request $request)
    {
        Mitra::create($request->all());
        return redirect('mitra')->with('status', 'Produk Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit1($no)
    {
        $item = DB::table('mitra_bekasi')->where('no', $no)->first();
        return view('mitra.edit1', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update1(Request $request, $no)
    {
        $item = DB::table('mitra_bekasi')
            ->where('no', $no)
            ->update([
                'nama_toko' => $request->nama_toko,
                'alamat' => $request->alamat,
                'no_telpon' => $request->no_telpon
            ]);
        return redirect('mitra')->with('status', 'Kemitraan Berhasil Diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete1($no)
    {
        DB::table('mitra_bekasi')->where('no', $no)->delete();
        return redirect('mitra')->with('status', 'Mitra Berhasil Dihapus!');
    }
}
