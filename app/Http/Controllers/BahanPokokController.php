<?php

namespace App\Http\Controllers;

use App\Models\Bahan_pokok;
use App\Models\Pesanan;
use App\Models\PesananDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BahanPokokController extends Controller
{

    public function __construct()
    {
        $this->Bahan_pokok = new Bahan_pokok();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bahan_pokok = \App\Models\Bahan_pokok::all();

        return view('layout.produk', ['bahan_pokok' => $bahan_pokok]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layout.tambah1');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Bahan_pokok::create($request->all());
        return redirect('layout/produk')->with('status', 'Produk Berhasil Ditambahkan!');
    }


    public function edit($kode_produk)
    {
        $item = DB::table('bahan_pokok')->where('kode_produk', $kode_produk)->first();
        return view('layout.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_produk)
    {
        $item = DB::table('bahan_pokok')
        ->where('kode_produk', $kode_produk)
        ->update([
            'nama_produk' => $request->nama_produk,
            'harga_awal' => $request->harga_awal,
            'harga_diskon' => $request->harga_diskon,
            'keterangan' => $request->keterangan
        ]);
        return redirect('layout/produk')->with('status', 'Produk Berhasil Diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($kode_produk)
    {
        DB::table('bahan_pokok')->where('kode_produk', $kode_produk)->delete();
        return redirect('layout/produk')->with('status', 'Produk Berhasil Dihapus!');
    }
}
