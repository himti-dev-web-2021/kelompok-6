<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\helper;
use App\Models\PesananDetail;
use App\Models\pengguna;
use App\Models\keranjang;
use Illuminate\Support\Facades\DB;

class DetailPemesananController extends Controller
{
    public function datauser(Request $request)
    {
        DB::table('pesanan_details')->insert([
            'id_user' => $request->id_user,
            'nama_produk' => $request->nama_produk,
            'jumlah' => $request->jumlah,
            'total_harga' => $request->total_harga,
            'id_produk' => $request->id_produk,
            'id_pemesanan' => $request->user_id
        ]);
        return redirect()->intended('Keranjang');
    }
    public function tampilProduk()
    {
        $user_id = Helper::IDGenerator(new PesananDetail, 'id_pemesanan', 6, 'KLTG-BP');
        $pesanans = \App\Models\keranjang::all();
        return view('DetailOrder', ['pesanans' => $pesanans, 'user_id' => $user_id]);
    }
}
