<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ListmitraController extends Controller
{
    public function indexID()
    {

        $mitra_bekasi = \App\Models\Mitra::all();

        return view('.listmitraID', ['mitra_bekasi' => $mitra_bekasi]);
    }
    public function index()
    {

        $mitra_bekasi = \App\Models\Mitra::all();

        return view('/listmitra', ['mitra_bekasi' => $mitra_bekasi]);
    }
}
